package View;
import java.awt.*;
import java.awt.event.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.*;

public class View extends JPanel {
	JFrame frame = new JFrame ("Simple Frame");
	JPanel p = new JPanel();
	JButton b15 = new JButton("START");
	JPanel panel1 = new JPanel(null);
	JPanel panel2 = new JPanel(null);
	JPanel panel3 = new JPanel(null);
	JPanel panel4 = new JPanel(null);
	JPanel panel5 = new JPanel(null);
	JPanel panel6 = new JPanel(null);
	JPanel panel7 = new JPanel(null);
	JPanel panel8 = new JPanel(null);
	JPanel panel9 = new JPanel(null);
	JLabel labelTime = new JLabel("");
	//elemente pentru panel2
	JTextField textField20= new JTextField("					");
	JTextField textField25= new JTextField("					");

	public void setTextField20(String string) {
		this.textField20.setText(string);;
	}

	public void setTextField25(String string) {
		this.textField25.setText(string);
	}

	public void setTextField23(String string) {
		this.textField23.setText(string);
	}

	public void setTextField24(String string) {
		this.textField24.setText(string);
	}
	
	JTextField textField23= new JTextField("					");
	JTextField textField24= new JTextField("					");
	JLabel labelP20 = new JLabel ("Coada1");
	JLabel labelP21 = new JLabel ("Coada2");
	JLabel labelP22 = new JLabel ("Coada3");
	JLabel labelP23 = new JLabel ("Coada4");
	
	JTextField textField01= new JTextField("	");
	JTextField textField02= new JTextField("	");
	JTextField textField03= new JTextField("	");
	JTextField textField04= new JTextField("	");
	JTextField textField05= new JTextField("	");
	JTextField textField06= new JTextField("	");
	JTextField textField07= new JTextField("	");
	JTextField textField08= new JTextField("	");
	JTextField textField09= new JTextField("	");
	JTextField textField10= new JTextField("	");
	JTextField textField1 = new JTextField("	");
	JTextField textField2 = new JTextField("	");
	JTextField textField4 = new JTextField("	");
	JTextField textField5 = new JTextField("	");
	JTextField textField6 = new JTextField("	");
	JTextField textField7 = new JTextField("	");
	
	JLabel labelP61 = new JLabel ("AvgWaiting");
	JLabel labelP64 = new JLabel ("AvgWaiting");
	JLabel labelP67 = new JLabel ("AvgWaiting");
	JLabel labelP1 = new JLabel ("AvgWaiting");
	
	JLabel labelP62 = new JLabel ("AvgService");
	JLabel labelP65 = new JLabel ("AvgService");
	JLabel labelP68 = new JLabel ("AvgService");
	JLabel labelP2 = new JLabel ("AvgService");
	
	JLabel labelP63 = new JLabel ("PeakHour");
	JLabel labelP66 = new JLabel ("PeakHour");
	JLabel labelP69 = new JLabel ("PeakHour");
	JLabel labelP3 = new JLabel ("PeakHour");
	
	JLabel labelP70 = new JLabel ("Empty");
	JLabel labelP71 = new JLabel ("Empty");
	JLabel labelP72 = new JLabel ("Empty");
	JLabel labelP73 = new JLabel ("Empty");
	public void setTextField4(String string) {
		this.textField4.setText(string);
	}

	public void setTextField7(String string) {
		this.textField7.setText(string);
	}
	
	public void setTextField5(String string) {
		this.textField5.setText(string);
	}

	public void setTextField6(String string) {
		this.textField6.setText(string);
	}

	public void setLabelP70(JLabel labelP70) {
		this.labelP70 = labelP70;
	}

	public void setLabelP71(JLabel labelP71) {
		this.labelP71 = labelP71;
	}

	public void setLabelP72(JLabel labelP72) {
		this.labelP72 = labelP72;
	}

	public void setLabelP73(JLabel labelP73) {
		this.labelP73 = labelP73;
	}

	public void setTextField3(JTextField textField3) {
		this.textField3 = textField3;
	}

	//
	JTextField textField11 = new JTextField("2");
	JLabel label11 = new JLabel ("Arriving");
	JTextField textField12 = new JTextField("7");
	
	
	JTextField textField21 = new JTextField("4");
	JLabel label21 = new JLabel ("Service Time");
	JTextField textField22 = new JTextField("9");
	
	JTextField textField3 = new JTextField("4");
	JLabel label3 = new JLabel ("Number of queues");
	
	JTextField textField41 = new JTextField("2");
	JLabel label41 = new JLabel ("Simulation Time");
	JTextField textField42 = new JTextField("3");
	
	public String getArrivingMin()
	{
		return textField11.getText();
	}
	
	public void setTextField01(String string) {
		this.textField01.setText(string);
	}

	public void setTextField02(String string) {
		this.textField02.setText(string);
	}

	public void setTextField03(String textField03) {
		this.textField03.setText(textField03);
	}

	public void setTextField04(String textField04) {
		this.textField04.setText(textField04);
	}

	public void setTextField05(String textField05) {
		this.textField05.setText(textField05);
	}

	public void setTextField06(String textField06) {
		this.textField06.setText(textField06);
	}

	public void setTextField07(String textField07) {
		this.textField07.setText(textField07);
	}

	public void setTextField08(String textField08) {
		this.textField08.setText(textField08);
	}

	public void setTextField09(String textField09) {
		this.textField09.setText(textField09);
	}

	public void setTextField10(String textField10) {
		this.textField10.setText(textField10);
	}

	public void setTextField1(String peak1) {
		this.textField1.setText(peak1);
	}

	public void setTextField2(String textField2) {
		this.textField2.setText(textField2);
	}

	public String getLabelTime() {
		return labelTime.getText();
	}
	
	public String getArrivingMax()
	{
		return textField12.getText();
	}
	
	public String getServiceTimeMin()
	{
		return textField21.getText();
	}
	
	public String getServiceTimeMax()
	{
		return textField22.getText();
	}
	
	public String getNoQueues()
	{
		return textField3.getText();
	}
	
	public String getSimulationMin()
	{
		return textField41.getText();
	}
	
	public String getSimulationMax()
	{
		return textField42.getText();
	}
	
//	public void setRezultat(String rezultat)
//	{
//		label5.setText(rezultat);
//	}

	public View(){
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(900, 720);
		
		//panel1
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		frame.add(panel1);
		labelTime.setFont( new Font("Times New Roman", Font.PLAIN, 20));
		panel1.add(labelTime);
		
		label11.setFont( new Font("Times New Roman", Font.PLAIN, 20));
		textField11.setMaximumSize(new Dimension(400,25));
		textField12.setMaximumSize(new Dimension(400,25));
		panel1.add(label11);
		panel1.add(textField11);
		panel1.add(textField12);
	
		label21.setFont( new Font("Times New Roman", Font.PLAIN, 20));
		textField21.setMaximumSize(new Dimension(400,25));
		panel1.add(label21);
		panel1.add(textField21);
		
		textField22.setMaximumSize(new Dimension(400,25));
		panel1.add(textField22);
		
		label3.setFont( new Font("Times New Roman", Font.PLAIN, 20));
		textField3.setMaximumSize(new Dimension(400,25));
		panel1.add(label3);
		panel1.add(textField3);
		
		label41.setFont( new Font("Times New Roman", Font.PLAIN, 20));
		textField41.setMaximumSize(new Dimension(400,25));
		frame.add(panel1);
		panel1.add(label41);
		panel1.add(textField41);
		
		textField42.setMaximumSize(new Dimension(400,25));
		panel1.add(textField42);
		 
		b15.setPreferredSize(new Dimension(40, 40));
		panel1.add(b15);
		//panel1.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel1.setBackground(new Color(179, 204, 204));
		
		//panel2
		
		labelP61.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP62.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP63.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP64.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP65.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP66.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP67.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP68.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP69.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP70.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP71.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP72.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP73.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP1.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP2.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		labelP3.setFont( new Font("Times New Roman", Font.PLAIN, 15));
		
		//frame.add(panel2);
		panel2.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel6.setLayout(new FlowLayout(FlowLayout.LEFT));
		labelP20.setFont( new Font("Times New Roman", Font.PLAIN, 20));
		textField20.setMaximumSize(new Dimension(400,25));
		
		textField01.setMaximumSize(new Dimension(400,25));
		textField02.setMaximumSize(new Dimension(400,25));
		textField03.setMaximumSize(new Dimension(400,25));
		
		panel2.add(labelP20);
		panel2.add(textField20);
		
		panel6.add(labelP61);
		panel6.add(textField01);
		panel6.add(labelP62);
		panel6.add(textField02);
		panel6.add(labelP63);
		panel6.add(textField03);
		panel6.add(labelP70);
		panel6.add(textField4);
		
		panel3.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel7.setLayout(new FlowLayout(FlowLayout.LEFT));
		labelP21.setFont( new Font("Times New Roman", Font.PLAIN, 20));
		textField23.setMaximumSize(new Dimension(400,25));
		textField04.setMaximumSize(new Dimension(400,25));
		textField05.setMaximumSize(new Dimension(400,25));
		textField06.setMaximumSize(new Dimension(400,25));
		
		panel3.add(labelP21);
		panel3.add(textField23);
		
		panel7.add(labelP64);
		panel7.add(textField04);
		panel7.add(labelP65);
		panel7.add(textField05);
		panel7.add(labelP66);
		panel7.add(textField06);
		panel7.add(labelP71);
		panel7.add(textField5);
		
		panel4.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel8.setLayout(new FlowLayout(FlowLayout.LEFT));
		labelP22.setFont( new Font("Times New Roman", Font.PLAIN, 20));
		textField24.setMaximumSize(new Dimension(400,25));
		textField07.setMaximumSize(new Dimension(400,25));
		textField08.setMaximumSize(new Dimension(400,25));
		textField09.setMaximumSize(new Dimension(400,25));
		textField4.setMaximumSize(new Dimension(400,25));
		textField5.setMaximumSize(new Dimension(400,25));
		textField6.setMaximumSize(new Dimension(400,25));
		textField7.setMaximumSize(new Dimension(400,25));
		
		panel4.add(labelP22);
		panel4.add(textField24);
		panel8.add(labelP67);
		panel8.add(textField07);
		panel8.add(labelP68);
		panel8.add(textField08);
		panel8.add(labelP69);
		panel8.add(textField09);
		panel8.add(labelP72);
		panel8.add(textField6);
		
		panel5.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel9.setLayout(new FlowLayout(FlowLayout.LEFT));
		labelP23.setFont( new Font("Times New Roman", Font.PLAIN, 20));
		textField25.setMaximumSize(new Dimension(400,25));
		textField10.setMaximumSize(new Dimension(400,25));
		textField1.setMaximumSize(new Dimension(400,25));
		textField2.setMaximumSize(new Dimension(400,25));
		
		panel5.add(labelP23);
		panel5.add(textField25);
		panel9.add(labelP1);
		panel9.add(textField10);
		panel9.add(labelP2);
		panel9.add(textField1);
		panel9.add(labelP3);
		panel9.add(textField2);
		panel9.add(labelP73);
		panel9.add(textField7);
		
		panel2.setBackground(new Color(179, 204, 204));
		panel3.setBackground(new Color(179, 204, 204));
		panel4.setBackground(new Color(179, 204, 204));
		panel5.setBackground(new Color(179, 204, 204));
		panel6.setBackground(new Color(179, 204, 204));
		panel7.setBackground(new Color(179, 204, 204));
		panel8.setBackground(new Color(179, 204, 204));
		panel9.setBackground(new Color(179, 204, 204));
		
		p.add(panel1);
		p.add(panel2);
		p.add(panel6);
		p.add(panel3);
		p.add(panel7);
		p.add(panel4);
		p.add(panel8);
		p.add(panel5);
		p.add(panel9);
		p.setBackground(new Color(179, 204, 204));
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		frame.setContentPane(p); 
		frame.setVisible(true); 
		
		clock();
	}
	
	public void clock()
	{
		Thread clock = new Thread()
		{
			public void run()
			{
				try {
					for(int i = 0; i < 60; i++)
					{
						Calendar calendar = new GregorianCalendar();
						int minutes = (calendar.get(Calendar.SECOND));
						int hour = (calendar.get(Calendar.HOUR));
						labelTime.setText("Time is: "+hour+":"+minutes+ "\n");
						sleep(1000);
					}		
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		};
		clock.start();
	}
	public void addResultListener(ActionListener button15) {
		b15.addActionListener(button15);
	} 
}