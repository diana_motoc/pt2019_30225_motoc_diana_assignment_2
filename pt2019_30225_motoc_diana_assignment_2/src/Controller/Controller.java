package Controller;
import java.awt.event.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JTextField;

import Model.*;
import View.*;

public class Controller {
	private Model m_model;
	private View m_view;

	public Controller(Model model, View view) {
		m_model = model;
		m_view = view;
		this.m_view.addResultListener(new ResultListener());
	}
	
	class ResultListener implements ActionListener {
		public void afisareCeas()
		{
			
			Thread clock = new Thread()
			{
				public void run()
				{
					
					int cnt1 = 0;
					int cnt2 = 0;
					int cnt3 = 0;
					int cnt4 = 0;
					int peak1 = 0;
					int peak2 = 0;
					int peak3 = 0;
					int peak4 = 0;
					
					String peak1A = "";
					String peak2A = "";
					String peak3A = "";
					String peak4A = "";
					
					double avgService1 = 0;
					double avgService2 = 0;
					double avgService3 = 0;
					double avgService4 = 0;
					
					double avgWaiting1 = 0;
					double avgWaiting2 = 0;
					double avgWaiting3 = 0;
					double avgWaiting4 = 0;
					
					try {
						for(int i = 0; i < 30; i++)
						{
							for(int j = 0; j < m_model.getQueues().size(); j++)
							{
								if (m_model.getQueues().get(j).isEmpty() == 0) //nu e goala
								{
									if(j == 0)
									{
										m_view.setTextField20(m_model.getQueues().get(j).toString());
										if(m_model.getQueues().get(j).getSize() > peak1)
										{
											peak1 = m_model.getQueues().get(j).getSize();
											peak1A = m_view.getLabelTime();
										}
										
									}
									else if(j == 1)
									{
										m_view.setTextField23(m_model.getQueues().get(j).toString());
										if(m_model.getQueues().get(j).getSize() > peak2)
										{
											peak2 = m_model.getQueues().get(j).getSize();
											peak2A = m_view.getLabelTime();
										}
									}
									else if(j == 2)
									{
										m_view.setTextField24(m_model.getQueues().get(j).toString());
										if(m_model.getQueues().get(j).getSize() > peak3)
										{
											peak3 = m_model.getQueues().get(j).getSize();
											peak3A = m_view.getLabelTime();
										}
									}
									else if(j == 3)
									{
										m_view.setTextField25(m_model.getQueues().get(j).toString());
										if(m_model.getQueues().get(j).getSize() > peak4)
										{
											peak4 = m_model.getQueues().get(j).getSize();
											peak4A = m_view.getLabelTime();
										}
									}
								}
								else
								{
									if(j == 0)
									{
										m_view.setTextField20(" ");
										cnt1++;
									}
									else if(j == 1)
									{
										m_view.setTextField23(" ");
										cnt2++;
									}
									else if(j == 2)
									{
										m_view.setTextField24(" ");
										cnt3++;
									}
									else if(j == 3)
									{
										m_view.setTextField25(" ");
										cnt4++;
									}
								}
							}
								
							
							sleep(1000);
						}	
						
						avgService1 = m_model.getQueues().get(0).getServiceT() / peak1; 
						avgService2 = m_model.getQueues().get(1).getServiceT() / peak2; 
						avgService3 = m_model.getQueues().get(2).getServiceT() / peak3; 
						avgService4 = m_model.getQueues().get(3).getServiceT() / peak4; 
						
						avgWaiting1 =  m_model.getQueues().get(0).getWaitingT() / peak1;
						avgWaiting2 =  m_model.getQueues().get(1).getWaitingT() / peak2;
						avgWaiting3 =  m_model.getQueues().get(0).getWaitingT() / peak3;
						avgWaiting4 =  m_model.getQueues().get(0).getWaitingT() / peak4;
						
					
						m_view.setTextField03(peak1A);
						m_view.setTextField06(peak2A);
						m_view.setTextField09(peak3A);
						m_view.setTextField2(peak4A);
						m_view.setTextField02(avgService1 + "");
						m_view.setTextField05(avgService2 + "");
						m_view.setTextField08(avgService3 + "");
						m_view.setTextField1(avgService4 + "");
						m_view.setTextField4(cnt1 + "");
						m_view.setTextField5(cnt2 + "");
						m_view.setTextField6(cnt3 + "");
						m_view.setTextField7(cnt4 + "");
						m_view.setTextField01(avgWaiting1 + "");
						m_view.setTextField04(avgWaiting2 + "");
						m_view.setTextField07(avgWaiting3 + "");
						m_view.setTextField10(avgWaiting4 + "");
						
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			};
			clock.start();
		}
		
		public void actionPerformed(ActionEvent e) {
			
			
			//iau din text field-uri informatiile
			String minArriving = m_view.getArrivingMin();
			String maxArriving = m_view.getArrivingMax();
			String minServiceTime = m_view.getServiceTimeMin();
			String maxServiceTime = m_view.getServiceTimeMax();
			String numberQueues = m_view.getNoQueues();
			String minSimulation = m_view.getSimulationMin();
			String maxSimulation = m_view.getSimulationMax();

			int minArr, maxArr, minService, maxService, numberOfQueues;

			minArr = Integer.parseInt(minArriving);
			maxArr = Integer.parseInt(maxArriving);
			minService = Integer.parseInt(minServiceTime);
			maxService = Integer.parseInt(maxServiceTime);
			numberOfQueues = Integer.parseInt(numberQueues);

			for(int i = 0; i < numberOfQueues; i++)
			{
				m_model.getQueues().add(new MyQueue(minArr, maxArr, minService, maxService));
			}
			this.afisareCeas();	
		}
	}
}