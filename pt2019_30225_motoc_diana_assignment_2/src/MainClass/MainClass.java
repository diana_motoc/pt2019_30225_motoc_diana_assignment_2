package MainClass;

import Controller.Controller;
import Model.Customer;
import Model.*;
import View.View;

public class MainClass {

	public static void main(String[] args) {
		View m_view = new View();
		//Customer my_customer = new Customer(2, 20, 4, 15);
		//System.out.println(my_customer.toString());
		//MyQueue q = new MyQueue(2, 7);
		Model m_model = new Model();
		Controller m_controller = new Controller(m_model, m_view);
	}
}
