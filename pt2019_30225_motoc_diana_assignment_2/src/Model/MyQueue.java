package Model;
import java.awt.List;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JTextField;

public class MyQueue extends Thread{
	private int serviceT;
	private int lastOrder;
	private int maxArriving;
	private int minArriving;
	private int maxService;
	private int minService;
	private int waitingT;
	
	BlockingQueue<Customer> q = new LinkedBlockingQueue<Customer>();
	//private int clock = 0;
	
	public MyQueue(int minArriving, int maxArriving, int minService, int maxService)
	{
		this.minArriving = minArriving;
		this.maxArriving = maxArriving;
		this.minService = minService;
		this.maxService = maxService;
		Random rand = new Random();
		int n = rand.nextInt(7);
		for(int i = 0; i < n + 3; i ++)
		{
			Customer c = new Customer(minService, maxService, minArriving, maxArriving);
			c.setOrderNumber(i + 1);
			q.add(c);
		}
		
		lastOrder = 7;
		this.start();
	}
	
	public int getServiceT() {
		return serviceT;
	}
	
	public int getWaitingT() {
		return waitingT;
	}
	
	public void setWaitingT(int s) {
		this.waitingT = s;
	}

	public String toString()
	{
		
		String newwQ = "";
		if(q.size() != 0)
		{
			for(Customer i: q)
				
				newwQ += (i.toString() + " ");
		}
		return newwQ;
		/*String newwQueue = "";
		
		//q = (Queue<Customer>) Collections.unmodifiableCollection(q);
		Queue<Customer> p = this.myClone(q);
		
		
		if(p.size() != 0) {
		for(Customer i: p)
			
			newwQueue += (i.toString() + " ");
		}
		return newwQueue;*/
		
		/*
		String newwQueue = "";
		try {
			Queue<Customer> p = q.getClass().newInstance();
			if(q.size() !=0) {
			for(Customer i: q)
			{
				p.add(i);
				//newwQueue += (i.toString() + " ");
				//newwQueue += p.peek().toString() + " ";
				
			}
			}
			if(p.size() != 0) {
				for(Customer i: p)
					
					newwQueue += (i.toString() + " ");
				}
				
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return newwQueue;*/
		
		
		/*String neww = "";
		//Queue<Customer> p = new LinkedList<Customer>();
		Iterator<Customer> iterator = q.iterator();
		while(iterator.hasNext())
		{
			//Customer c = iterator.next();
			neww += (iterator.next().getOrderNumber() + " ");
			//iterator.remove();
		}
		return neww;*/
	}
	
	public Queue<Customer> myClone(Queue<Customer> a){
		Queue<Customer> neww = new LinkedList<Customer>();
		
		for(Customer i: a)
			neww.add(i);
		
		return neww;
	}
	
	public int getSize()
	{
		return q.size();
	}
	
	public int isEmpty()
	{
		if(q.isEmpty())
			return 1;
		return 0;
	}
	
	public void run()
	{
		Random rand = new Random();
		int j = 0;
		try {
			for(int i = 1; i <= 60; i++, j++)
			{
				
				Customer c1 = new Customer(minService, maxService, minArriving, maxArriving);
				if(q.isEmpty())
				{
					c1.setOrderNumber(1);			
				}
				else
				{
					c1.setOrderNumber(lastOrder + 1);	
				}

				if(i % c1.getArrivingTime()== 0)
				{
					q.add(c1);
					lastOrder++;
				}
	
				if(!q.isEmpty() && (j >= q.peek().getServiceTime()))
				{
					this.serviceT += q.peek().getServiceTime();
					q.remove();
					j = 0;
				}
				
				for(Customer c2: q)
				{
					if(c2 != q.peek())
					{
						c2.setWaitingTime(c2.getWaitingTime() + 1);
						this.setWaitingT(1+waitingT);
					}
				}
				sleep(1000);
			}		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}