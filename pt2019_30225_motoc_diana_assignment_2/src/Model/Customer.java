package Model;
import java.util.*;

public class Customer {
	private int serviceTime;
	private int orderNumber;
	//de pus campul arrivingTime aici
	private int arrivingTime;
	private int waitingTime;

	public Customer(int minService, int maxService, int minArriving, int maxArriving)
	{
		Random rand = new Random();
		this.serviceTime = rand.nextInt(maxService-minService) + minService;
		this.arrivingTime = rand.nextInt(maxArriving-minArriving) + minArriving;
	}

	public int getArrivingTime() {
		return arrivingTime;
	}
	
	public int getServiceTime() {
		return serviceTime;
	}

	public int getWaitingTime() {
		return waitingTime;
	}
	
	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}
	
	public void setWaitingTime(int waitingTime) {
		this.waitingTime = waitingTime;
	}


	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String toString()
	{
		return orderNumber + "";
	}
}
